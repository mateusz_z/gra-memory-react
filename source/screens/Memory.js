import React, { useState } from 'react';
import { View, Text, StyleSheet, Alert  } from 'react-native';
import Tile from '../components/Tile';
const Memory = () => {
    const [operationText, setOperationText,] = useState('');
    const [match, setMatch,] = useState('');
    const [shouldShow, setShouldShow] = useState(true);
    const buttonPress = (buttonValue) => {
       
         switch (buttonValue) {

            case 'start':
                setOperationText([]);
              setMatch (match + 'greenyellowredblue');
                generate(match);
                   
            

                break;
            case 'check':
                if(operationText==match){
                    
                    const createAlert = () =>{
                    Alert.alert(
                      "Wygrana",
                      "Gratulacje",)}
                }else{ 
                    const createAlert = () =>{
                    Alert.alert(
                      "Przegrana",
                      "Spróbuj ponownie",)
                    }}

                break;
            case 'red':
            case 'blue':
            case 'green':
            case 'yellow':
                setOperationText(operationText +  buttonValue)
                break; 
       


        }
    }
    
          const  hideMatchWithDelay=()=>{

            setTimeout(()=>{
                 setShouldShow(!shouldShow)
            }, 10000);
        
          }
          const generate=(match)=>{
            for(let i = 0; i < 6; i++){
                var RandomNumber = Math.floor(Math.random() * 4) + 1 ;
                 switch(RandomNumber){
                     case 1:
                         setMatch(match+'blue')
                         break;
                     case 2:
                         setMatch(match+'red')
                         break;
                     case 3:
                         setMatch(match+'green')
                         break; 
                     case 4:
                         setMatch(match+'yellow')
                         break;   
                 }}
          }

    return (
        <View style={styles.fullStyle}>
             {shouldShow ? (
            <Text style={styles.viewStyle}>{match}</Text>
            ) : true}
            <Text style={styles.viewStyle}>{operationText}</Text>
            <View style={styles.padStyle}>
                <Tile  buttonValue={'red'} onPressButton={buttonPress} color={'red'}></Tile>
                <Tile  buttonValue={''} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile  buttonValue={''} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile  buttonValue={'blue'} onPressButton={buttonPress} color={'blue'}></Tile>
            </View>
            <View style={styles.padStyle}>
                <Tile  buttonValue={'green'} onPressButton={buttonPress} color={'green'}></Tile>
                <Tile  buttonValue={''} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile  buttonValue={''} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile  buttonValue={'yellow'} onPressButton={buttonPress} color={'yellow'}></Tile>
            </View>
            <View style={styles.padStyle}>
                <Tile  buttonValue={''} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile  buttonValue={''} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile  buttonValue={''} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile  buttonValue={''} onPressButton={buttonPress} color={'white'}></Tile>
            </View>
            <View style={styles.padStyle}>
                <Tile  buttonValue={'start'} onPressButton={buttonPress,hideMatchWithDelay}  color={'white'}></Tile>
                <Tile  buttonValue={''} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile  buttonValue={''} onPressButton={buttonPress} color={'white'}></Tile>
                <Tile  buttonValue={'check'} onPressButton={buttonPress} color={'white'}></Tile>
            </View>
            
        </View>

    );
}


const styles = StyleSheet.create({
    viewStyle: {
        borderColor: 'grey',
        borderWidth: 1,
        alignContent: "stretch",
        alignItems: 'stretch',
        flex: 1,
        textAlign: 'center',
        fontSize: 20,

    },
    padStyle: {
        flex: 1,
        flexDirection: 'row',
        borderColor: 'grey',
        borderWidth: 1,
        alignContent: "stretch",
        alignItems: 'stretch',
        width: '100%',
        height: '15%',
        fontSize: 20
    },
    fullStyle: {
        flex: 1,
    },
    equalStyle: {
        backgroundColor: 'orange',
    },
});
export default Memory;
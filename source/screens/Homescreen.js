import React from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';

const HomeScreen = (prop) => {

return(
  <View>
    <Text style={styles.viewStyle}>Witaj. Zagramy?</Text>

     <Button
      title='Graj'
      onPress={()=> {
        prop.navigation.navigate('Memory');
      }}
    />
  </View>
);
}

const styles = StyleSheet.create({
  viewStyle: {
     
      textAlign: 'center',
      fontSize: 20,  }})

export default HomeScreen;
import HomeScreen from './source/screens/Homescreen';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Memory from './source/screens/Memory';


const AppNavigator = createStackNavigator({
  Home: {screen: HomeScreen},
  Memory: {screen: Memory},
},
{
  initialRouteName: 'Home',
  defaultNavigationOptions: {title:'Start'}
});

export default createAppContainer(AppNavigator);